<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);
Route::post('login', 'Auth\LoginController@authenticate')->name('login');

Route::group(['middleware' => 'auth'], function () {

Route::get('getMenu','Controller@getMenu')->name('role.getMenu');

Route::prefix('employee')->group(function () {
    Route::get('index', 'EmployeeController@index')->name('employee.index');
    Route::get('data', 'EmployeeController@data')->name('employee.data');
    Route::get('form', 'EmployeeController@form')->name('employee.form');

});

// DASHBOARD
Route::get('data/dashboard/{id}', 'HomeController@data')->name('data.dashboard');
Route::post('/endSession', 'Controller@endSession');


});
