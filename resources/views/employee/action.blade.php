<script>


$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

var act_url = '{{ route('employee.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": [0,1] }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'cek', name: 'cek' },
        { data: 'action', name: 'action' },
        { data: 'nip_no', name: 'nip_no' },
        { data: 'nik_no', name: 'nik_no' },
        { data: 'npwp_no', name: 'npwp_no' },
        { data: 'full_name', name: 'full_name' },
        { data: 'alias_name', name: 'alias_name' },
        { data: 'birth_place', name: 'birth_place' },
    ]
});

</script>
