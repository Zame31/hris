<script type="text/javascript">
    // enable enter in form login
    $('#loginForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            login();
        }
    });

    // disable enter in form reset password
    $('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            return false;
        }
    });

    $(document).ready(function () {

        // validate reset password
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_password: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            min: 6,
                            message: 'Silahkan isi minimal 6 karakter'
                        }
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        // validate login
        $("#loginForm").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

    function login(){
        var validateLogin = $('#loginForm').data('bootstrapValidator').validate();
        if (validateLogin.isValid()) {

            var formData = document.getElementById("loginForm");
            var objData = new FormData(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('login') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $("#alertInfo").addClass('d-none');
                    $("#alertError").addClass('d-none');
                    $("#alertSuccess").addClass('d-none');
                    $("#loading").css('display', 'block');
                },

                success: function (response) {
                    console.log(response);
                    $("#loading").css('display', 'none');
                    switch (response.rc) {
                        // password / username invalid
                        case 0:
                            $("#inputUsername").val('');
                            $("#inputPassword").val('');
                            $("#alertError").removeClass('d-none');
                            $("#alertError").text(response.rm);
                        break;

                        // akun tidak aktif
                        case 1:
                            $("#inputUsername").val('');
                            $("#inputPassword").val('');
                            $("#alertInfo").removeClass('d-none');
                            $("#alertInfo").text(response.rm);
                        break;

                        // reset password
                        case 2:
                            $("#inputUsername").val('');
                            $("#inputPassword").val('');
                            $("#form-data")[0].reset();
                            $('#form-data').bootstrapValidator("resetForm", true);
                            $("#modal").modal('show');
                            $("#id").val(response.id_user);
                        break;

                        // login success
                        case 3:
                            window.location.href = '{{ route('home') }}';
                        break;
                    }
                }

            }).done(function (msg) {
                $("#loading").css('display', 'none');
            }).fail(function (msg) {
                $("#loading").css('display', 'none');
                // toastr.error("Terjadi Kesalahan");
            });
        }
    }

    function resetPassword() {
        var validatePassword = $('#form-data').data('bootstrapValidator').validate();
        if (validatePassword.isValid()) {

            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('reset_password') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $("#alertInfo").addClass('d-none');
                    $("#alertError").addClass('d-none');
                    $("#alertSuccess").addClass('d-none');
                },

                success: function (response) {
                    console.log(response);
                    $("#modal").modal('hide');
                    window.location.href = '{{ route('login') }}';
                    toastr.success(response.rm);
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                toastr.error("Terjadi Kesalahan");
            });
        }

    }

</script>
