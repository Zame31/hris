<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\InventoryModel;
use App\Models\InventoryScModel;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'employee.list');
        }else {
            return view('master.master')->nest('child', 'employee.list');
        }
    }

    public function form(Request $request)
    {
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'employee.form');
        }else {
            return view('master.master')->nest('child', 'employee.form');
        }
    }


    public function data()
    {
       $data = \DB::select("SELECT * from master_employee order by updated_at");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>
              <a class="dropdown-item">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->id.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })

        ->rawColumns(['cek', 'action'])
        ->make(true);

    }



}
