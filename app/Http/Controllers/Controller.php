<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\MasterTxModel;
use Auth;
use App\Models\InventoryModel;
use App\Models\InventoryScModel;
use App\Models\BDDModel;
use App\Models\BDDScModel;



class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function numFormat($nominal)
    {
        return number_format($nominal,2,",",".");
    }

    public function clearSeparator($nominal)
    {
        return str_replace('.','',$nominal);
    }


    public function MonthIndo($data)
    {
        $bulan = array (
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return $bulan[$data];
    }

    public function endSession()
    {
        Auth::logout();
        // return redirect()->route('login');
        return response()->json([
            'rc' => 0,
            'rm' => "sukses"
        ]);
    }


    public function actSendApproval($setTable,$type,$datas){

        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (1,3)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 2]);
            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 2]);

            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actGiveApproval($setTable,$type,$datas,$memo){






            if($type=='semua'){
                $results = \DB::select("select * from ".$setTable." where id_workflow in (2)");

                foreach ($results as $item) {
                    DB::table($setTable)
                    ->where('id', $item->id)
                    ->update(['id_workflow' => 9]);

                    $getTxCode = $this->setJurnalData($item->id,$setTable);

                    // SET MEMO
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:s:i'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                    // UDPATE STATUS PAID INVENTORY
                    if ($setTable == 'master_inventory_schedule') {
                        DB::table('master_inventory_schedule')
                        ->where('id', $item->id)
                        ->update(['paid_status_id' => 1]);
                    }

                    // UDPATE STATUS PAID BDD
                    if ($setTable == 'master_bdd_schedule') {
                        DB::table('master_bdd_schedule')
                        ->where('id', $item->id)
                        ->update(['paid_status_id' => 1]);
                    }

                }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

            }else{
                foreach ($datas as $item) {

                DB::table($setTable)
                    ->where('id', $item)
                    ->update(['id_workflow' => 9]);

                    $getTxCode = $this->setJurnalData($item,$setTable);

                    // SET MEMO
                    $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                    DB::table('master_memo')->insert(
                        [
                            'id' => $get->max_id+1,
                            'tx_code' => $getTxCode,
                            'notes' => $memo,
                            'id_workflow' => 9,
                            'created_at' => date('Y-m-d H:s:i'),
                            'user_crt_id' => Auth::user()->id
                        ]
                    );

                    // UDPATE STATUS PAID
                    if ($setTable == 'master_inventory_schedule') {
                        DB::table('master_inventory_schedule')
                        ->where('id', $item)
                        ->update(['paid_status_id' => 1]);
                    }

                    // UDPATE STATUS PAID BDD
                    if ($setTable == 'master_bdd_schedule') {
                        DB::table('master_bdd_schedule')
                        ->where('id', $item)
                        ->update(['paid_status_id' => 1]);
                    }

                }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);
            }

    }

    public function actDeleteApproval($setTable,$type,$datas){
        if($type=='semua'){

            $results = \DB::select("select * from ".$setTable." where id_workflow in (1,3) ");

            foreach ($results as $item) {

                if ($setTable == 'master_inventory') {
                    InventoryModel::destroy($item->id);
                    InventoryScModel::where('inventory_id', $item->id)->delete();
                }else {
                    BDDModel::destroy($item->id);
                    BDDScModel::where('bdd_id', $item->id)->delete();
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }else{
            foreach ($datas as $item) {
                if ($setTable == 'master_inventory') {
                    InventoryModel::destroy($item);
                    InventoryScModel::where('inventory_id', $item)->delete();
                }else {
                    BDDModel::destroy($item);
                    BDDScModel::where('bdd_id', $item)->delete();
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function actDeleteApprovalRev($setTable,$type,$datas){
        if($type=='semua'){

            $results = \DB::select("select * from ".$setTable." where id_workflow in (9) ");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 3]);
            }

            return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {
                DB::table($setTable)
                 ->where('id', $item)
                 ->update(['id_workflow' => 3]);
             }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actGiveDeleteApprovalRev($setTable,$type,$datas,$memo){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (3)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 12]);

                $getTxCode = $this->setJurnalDataReversal($item->id,$setTable);

                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:s:i'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

                // UDPATE STATUS PAID
                if ($setTable == 'master_inventory_schedule') {
                    DB::table('master_inventory_schedule')
                    ->where('id',  $item->id)
                    ->update(['paid_status_id' => 0]);
                }

                 // UDPATE STATUS PAID BDD
                 if ($setTable == 'master_bdd_schedule') {
                    DB::table('master_bdd_schedule')
                    ->where('id',  $item->id)
                    ->update(['paid_status_id' => 0]);
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 12]);

                $this->setJurnalDataReversal($item,$setTable);

                 // UDPATE STATUS PAID
                 if ($setTable == 'master_inventory_schedule') {
                    DB::table('master_inventory_schedule')
                    ->where('id', $item)
                    ->update(['paid_status_id' => 0]);
                }

                 // UDPATE STATUS PAID BDD
                 if ($setTable == 'master_bdd_schedule') {
                    DB::table('master_bdd_schedule')
                    ->where('id', $item)
                    ->update(['paid_status_id' => 0]);
                }

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actRejectDeleteApprovalRev($setTable,$type,$datas){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (3)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 9]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 9]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actRejectApproval($setTable,$type,$datas){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (2)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 1]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 1]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

    public function actRejectCancelPayment($setTable,$type,$datas){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (14)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 9]);
            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 9]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }


    public function setJurnalData($item,$setTable)
    {

        if ($setTable == 'master_inventory_schedule') {
            $gData = collect(\DB::select("select * from master_inventory_schedule where id = ".$item))->first();
            $mInventory = collect(\DB::select("select * from master_inventory where id = ".$gData->inventory_id))->first();
            $invoice_type_id = 5;
            $set_invoice_id = $mInventory->inventory_type_id;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";

        }elseif ($setTable == 'master_inventory') {
            $gData = collect(\DB::select("select * from master_inventory where id = ".$item))->first();
            $invoice_type_id = 3;
            $set_invoice_id = $gData->inventory_type_id;
            $set_tx_amount = $gData->purchase_amount;
            $set_tx_notes = $gData->inventory_desc;

        }elseif ($setTable == 'master_bdd') {
            $gData = collect(\DB::select("select * from master_bdd where id = ".$item))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->bdd_amount;
            $set_tx_notes = $gData->bdd_note;

            $afi_acc_no = $gData->afi_acc_no;

        }elseif ($setTable == 'master_bdd_schedule') {
            $gData = collect(\DB::select("select * from master_bdd_schedule where id = ".$item))->first();
            $mBdd = collect(\DB::select("select * from master_bdd where id = ".$gData->bdd_id))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";
            $afi_acc_no = $mBdd->afi_acc_no;
        }


        if ($setTable == 'master_inventory_schedule' || $setTable == 'master_inventory')
        {


            if ($setTable == 'master_inventory') {
                $ref_debet = collect(\DB::select("select * from ref_bank_account
                where id = ".$gData->afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;
            } else {
                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            }


            $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();
        }
        elseif($setTable == 'master_bdd_schedule' || $setTable == 'master_bdd')
        {



            $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 "))->first();

            $ref_kredit = collect(\DB::select("select * from ref_bank_account
            where id = ".$afi_acc_no))->first();
            $ref_kredit->tx_notes = $ref_kredit->definition;

            // $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            // where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 "))->first();

        }

        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();

        // SET TX CODE
        $txcode='';
        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".$systemDate->current_date."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
        $prx=date("ymd", strtotime($systemDate->current_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){
          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
        }else{
            $txcode=$prx.$prx_branch.$prx_company."0001";
        }

        $tx_code = $txcode;
        // $tx_code =date('Ymd').Auth::user()->company_id.Auth::user()->branch_id;

        // tahun/bulan/tanggal/id_company/id_branch/empat digit terakhir master tx

        for ($i=0; $i <= 1; $i++) {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();

            $dsc = new MasterTxModel();
            $dsc->id = $get->max_id+1;
            $dsc->tx_code = $tx_code;
            $dsc->id_rel = $gData->id;

            $dsc->tx_date = $systemDate->current_date;
            $dsc->tx_type_id = $i;

            $dsc->tx_amount = $set_tx_amount;
            // DEBET
            if ($i == 1) {
                $coa = collect(\DB::select("select * from master_coa where coa_no='".$ref_debet->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_debet->coa_no;

                $dsc->tx_notes = $ref_debet->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('coa_no', $ref_debet->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            // KREDIT
            else{


                $coa = collect(\DB::select("select * from master_coa where coa_no='".$ref_kredit->coa_no."'"))->first();


                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_kredit->coa_no;

                $dsc->tx_notes = $ref_kredit->tx_notes.", ".$set_tx_notes;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('coa_no', $ref_kredit->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            $dsc->tx_segment_id = 1;
            $dsc->created_at = date('Y-m-d H:s:i');
            $dsc->user_crt_id = Auth::user()->id;
            $dsc->user_app_id = Auth::user()->id;
            $dsc->id_workflow = 9;
            $dsc->branch_id = 1;
            $dsc->company_id = 1;
            $dsc->save();
        }

        return $tx_code;


    }


    public function setJurnalDataReversal($item,$setTable)
    {


        if ($setTable == 'master_inventory_schedule') {
            $gData = collect(\DB::select("select * from master_inventory_schedule where id = ".$item))->first();
            $mInventory = collect(\DB::select("select * from master_inventory where id = ".$gData->inventory_id))->first();
            $invoice_type_id = 5;
            $set_invoice_id = $mInventory->inventory_type_id;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";

        }elseif ($setTable == 'master_inventory') {
            $gData = collect(\DB::select("select * from master_inventory where id = ".$item))->first();
            $invoice_type_id = 3;
            $set_invoice_id = $gData->inventory_type_id;
            $set_tx_amount = $gData->purchase_amount;
            $set_tx_notes = $gData->inventory_desc;

        }elseif ($setTable == 'master_bdd') {
            $gData = collect(\DB::select("select * from master_bdd where id = ".$item))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->bdd_amount;
            $set_tx_notes = $gData->bdd_note;

            $afi_acc_no = $gData->afi_acc_no;

        }elseif ($setTable == 'master_bdd_schedule') {
            $gData = collect(\DB::select("select * from master_bdd_schedule where id = ".$item))->first();
            $mBdd = collect(\DB::select("select * from master_bdd where id = ".$gData->bdd_id))->first();
            $invoice_type_id = 4;
            $set_tx_amount = $gData->amor_amount;
            $set_tx_notes = "";
            $afi_acc_no = $mBdd->afi_acc_no;

        }

        if ($setTable == 'master_inventory_schedule' || $setTable == 'master_inventory')
        {
            if ($setTable == 'master_inventory') {
                $ref_debet = collect(\DB::select("select * from ref_bank_account
                where id = ".$gData->afi_acc_no))->first();
                $ref_debet->tx_notes = $ref_debet->definition;
            } else {
                $ref_debet = collect(\DB::select("select * from ref_tx_invoice
                where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 and invoice_id = ".$set_invoice_id))->first();
            }

            $ref_kredit = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 0 and invoice_id = ".$set_invoice_id))->first();

        }
        elseif($setTable == 'master_bdd_schedule' || $setTable == 'master_bdd')
        {
            $ref_debet = collect(\DB::select("select * from ref_tx_invoice
            where invoice_type_id = ".$invoice_type_id." and tx_type_id = 1 "))->first();

            $ref_kredit = collect(\DB::select("select * from ref_bank_account
            where id = ".$afi_acc_no))->first();
            $ref_kredit->tx_notes = $ref_kredit->definition;
        }


        $systemDate = collect(\DB::select("select * from ref_system_date"))->first();


        // SET TX CODE
        $txcode='';
        $results = \DB::select("SELECT MAX(RIGHT(tx_code, 4)) as max_id FROM master_tx where tx_date='".$systemDate->current_date."' and branch_id=".Auth::user()->branch_id." and company_id=".Auth::user()->company_id);
        $prx=date("ymd", strtotime($systemDate->current_date));
        $prx_branch=Auth::user()->branch_id;
        $prx_company=Auth::user()->branch_id;

        if($results){
          $id_max= $results[0]->max_id;
          $sort_num = (int) substr($id_max, 1, 4);
          $sort_num++;
          $new_code = $prx.$prx_branch.$prx_company.sprintf("%04s", $sort_num);
          $txcode=$new_code;
        }else{
            $txcode=$prx.$prx_branch.$prx_company."0001";
        }

        $tx_code = $txcode;

        for ($i=0; $i <= 1; $i++) {
            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_tx"))->first();

            $dsc = new MasterTxModel();
            $dsc->id = $get->max_id+1;
            $dsc->tx_code = $tx_code;
            $dsc->id_rel = $gData->id;

            $dsc->tx_date = $systemDate->current_date;

            $dsc->tx_notes = "rev-".$ref_debet->tx_notes.", ".$set_tx_notes;
            $dsc->tx_amount = $set_tx_amount;
            $dsc->tx_type_id = $i;
            // DEBET
            if ($i == 1) {
                $coa = collect(\DB::select("select * from master_coa where coa_no='".$ref_debet->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 1"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_debet->coa_no;


                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('coa_no', $ref_debet->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }
            // KREDIT
            else{
                $coa = collect(\DB::select("select * from master_coa where coa_no='".$ref_kredit->coa_no."'"))->first();
                $jurnalOp = collect(\DB::select("select * from ref_jurnal_opr where coa_type_id = '".$coa->coa_type_id."' and tx_type_id = 0"))->first();

                $dsc->coa_type_id = $coa->coa_type_id;
                $dsc->acc_last = $coa->last_os;
                $dsc->coa_id = $ref_kredit->coa_no;
                // $dsc->tx_type_id = 1;

                if ($jurnalOp->operator_sign == '+') {
                    $dsc->acc_os = $coa->last_os + $set_tx_amount;
                }else {
                    $dsc->acc_os = $coa->last_os - $set_tx_amount;
                }

                DB::table('master_coa')
                ->where('coa_no', $ref_kredit->coa_no)
                ->update(['last_os' => $dsc->acc_os]);
            }

            // dd($i,$dsc->coa_id);
            $dsc->tx_segment_id = 1;
            $dsc->created_at = date('Y-m-d H:s:i');
            $dsc->user_crt_id = Auth::user()->id;
            $dsc->user_app_id = Auth::user()->id;
            $dsc->id_workflow = 9;
            $dsc->branch_id = 1;
            $dsc->company_id = 1;
            $dsc->save();
        }

        return $tx_code;


    }

    public function actCancelPaySc($setTable,$type,$datas)
    {
        DB::table($setTable)
        ->where('id', $datas)
        ->update(['id_workflow' => 14]);
    }

    public function getMenu()
    {

        $data = \DB::select("SELECT * FROM ref_rel_menu
        left join ref_menu on ref_menu.code = ref_rel_menu.code_menu where id_role = ".Auth::user()->user_role_id);
        return json_encode($data);
    }

    public function actGiveApprovalCancelPaySc($setTable,$type,$datas,$memo){
        if($type=='semua'){
            $results = \DB::select("select * from ".$setTable." where id_workflow in (14)");

            foreach ($results as $item) {
                DB::table($setTable)
                ->where('id', $item->id)
                ->update(['id_workflow' => 12]);

                $getTxCode = $this->setJurnalDataReversal($item->id,$setTable);

                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:s:i'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );

                    DB::table('master_inventory_schedule')
                    ->where('id', $item->id)
                    ->update(['paid_status_id' => 0]);


            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);

        }else{
            foreach ($datas as $item) {

               DB::table($setTable)
                ->where('id', $item)
                ->update(['id_workflow' => 12]);

                $getTxCode = $this->setJurnalDataReversal($item,$setTable);

                // SET MEMO
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_memo"))->first();
                DB::table('master_memo')->insert(
                    [
                        'id' => $get->max_id+1,
                        'tx_code' => $getTxCode,
                        'notes' => $memo,
                        'id_workflow' => 9,
                        'created_at' => date('Y-m-d H:s:i'),
                        'user_crt_id' => Auth::user()->id
                    ]
                );


                DB::table('master_inventory_schedule')
                ->where('id', $item)
                ->update(['paid_status_id' => 0]);

            }

        return json_encode(['rc'=>1,'rm'=>'berhasil']);
        }
    }

}
