var base_url = '//' + window.location.host;

$.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function convertToRupiah (objek) {


 if(objek.value==0){
    objek.value='';
 }else{
 var value = objek.value;
 value = value.replace(/^(0*)/,"");
 objek.value=value;

 separator = ".";
 a = objek.value;
 b = a.replace(/[^\d]/g, "");
 c = "";
 panjang = b.length;
 j = 0; for (i = panjang; i > 0; i--) {
     j = j + 1; if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c; } else {
             c = b.substr(i-1,1) + c; } } objek.value = c;
 }

}

function huruf(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
        return false;
    return true;
}

function znIconbox(tittle,text,action,color) {
    $('#zn-iconbox-tittle').html(tittle);
    $('#zn-iconbox-text').html(text);
    $('#zn-iconbox-action').html(action);
    $('#zn-iconbox').modal('show');

    $('#zn-iconbox-color').removeClass('kt-iconbox--warning');
    $('#zn-iconbox-color').removeClass('kt-iconbox--danger');
    $('#zn-iconbox-color').removeClass('kt-iconbox--success');
    if (color == 'warning') {
        $('#zn-iconbox-color').addClass('kt-iconbox--warning');
        $('#kt-iconbox__icon').html(` <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
        width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24" />
            <path
                d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                fill="#000000" opacity="0.3" />
            <path
                d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z"
                fill="#000000" />
            <path
                d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                fill="#000000" />
        </g>
    </svg>`);
    }else if (color == 'danger'){
        $('#zn-iconbox-color').addClass('kt-iconbox--danger');
        $('#kt-iconbox__icon').html(` <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24"/>
            <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
            <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
            <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"/>
            <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"/>
            <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"/>
            <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"/>
            <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"/>
            <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"/>
        </g>
    </svg>`);
    }else {
        $('#zn-iconbox-color').addClass('kt-iconbox--success');
        $('#kt-iconbox__icon').html(` <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
        width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24" />
            <path
                d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                fill="#000000" opacity="0.3" />
            <path
                d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z"
                fill="#000000" />
            <path
                d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                fill="#000000" />
        </g>
    </svg>`);
    }


}
function znIconboxClose(){
    $('#zn-iconbox').modal('hide');
}


function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}


function numFormat(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function loadNewPage(page) {

    $.ajax({
        type: "GET",
        url: page,
        data: {},
        success: function (data) {
            // $('#pageLoad').html(data).fadeIn();
        },
        beforeSend: function () {
            // animateCSS('#pageLoad', 'fadeOutDown');
            $('#pageLoad').fadeOut();
            // $("div#loading").show();
            loadingPage();
        },
    }).done(function (data) {
        endLoadingPage();
        var state = { name: "name", page: 'History', url: page };
        window.history.replaceState(state, "History", page);
        location.reload();
        $('#pageLoad').html(data).fadeIn();
        KTBootstrapDatepicker.init();
        $('#table_id').DataTable();
    $("#kt_aside_close_btn").click();



    });

}

function clearNumFormat(data) {
    return data.replace(/\./g,'');
}

function loadingPage() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });
}

function loadingModal() {
    KTApp.blockPage("#modal .modal-content", {
        overlayColor: "#000000",
        type: 'v2',
        state: "primary",
        message: "Processing..."
    })
}

function endLoadingModal() {
    KTApp.unblockPage("#modal .modal-content")
}

function endLoadingPage() {
    KTApp.unblockPage();
}

function currencyFormatter(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function convertDateFormat(date) {

    var new_date = new Date(date);

    var bulan = new_date.getMonth();
    var tanggal = ((new_date.getDate().toString().length == 1) ? '0' : '') + new_date.getDate();
    var tahun = new_date.getFullYear();
    var jam = ((new_date.getHours().toString().length == 1) ? '0' : '') + new_date.getHours();
    var minutes = ((new_date.getMinutes().toString().length == 1) ? '0' : '') + new_date.getMinutes();


    if (bulan > 36) {
        tahun += 3;
        bulan -= 36;
    }
    if (bulan > 24) {
        tahun += 2;
        bulan -= 24;
    }
    if (bulan > 12) {
        tahun += 1;
        bulan -= 12;
    }


    var bulan = ((bulan.toString().length == 1) ? '0' : '') + (bulan);

    // kondisi bulan februari
    if (bulan == '02') {
      if (tahun % 4 == 0) {
          // tahun kabisat
          if (tanggal > '29') {
              tanggal = '29';
          }
      } else {
          tanggal = '28';
      }
    }

    return tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + minutes;
  }

var KTBootstrapDatepicker = function () {

    return {
        init: function() {
            $('.init-date').datepicker({
                format: 'dd MM yyyy',
                autoclose: true,
            });


            $('.init-select2').select2({
                placeholder: "Silahkan Pilih"
            });

            $.sessionTimeout({
                title: 'Session Timeout Notification',
                message: 'Your session is about to expire.',
                logoutButton:false,
                warnAfter: (60000*10), //warn after 5 seconds
                redirAfter: (61000*10), //redirect after 10 secons,
                countdownMessage: 'Redirecting in {timer} seconds.',
                countdownBar: true,
                onRedir: function () {
                    endSession();
                }
            });

            avatar = new KTAvatar('kt_user_add_avatar');
        }
    };
}();

jQuery(document).ready(function() {
    KTBootstrapDatepicker.init();
    $('#table_id').DataTable();
});


  // PRINT
    // $(".znHeaderShow").css({"display": "none"});
    function closeCetakViewInvoice() {
        $('#zn-print').modal('hide');
        $('#detail').css('overflow', 'auto');
        $('#detail').modal('show');

        // $(".znHeaderShow").css({"display": "none"});
        $(".znSignature").css({"display": "none"});

        $("#svg").removeAttr('style');
        $(".znBtnShow").css({"display": "initial"});
        $(".kt-invoice__subtitle").css({"font-size": "13px","font-weight": "bold"});
        $(".kt-invoice__text").css({"font-size": "13px"});
        $(".kt-invoice__items").css({"margin-top": "10px"});
        $(".kt-invoice-1 .kt-invoice__body table tbody tr td").css({"padding": "1rem 0 1rem 0"});

    }

    function cetakViewInvoice() {
        // $(".znHeaderShow").css({"display": "block"});
        $(".znSignature").css({"display": "inherit"});
        $('#detail').modal('hide');

        $(".znBtnShow").css({"display": "none"});
        $(".kt-invoice__subtitle").css({"font-size": "10px","font-weight": "bold"});
        $(".kt-invoice__text").css({"font-size": "10px"});
        $(".kt-invoice__items").css({"margin-top": "10px"});
        $(".kt-invoice-1 .kt-invoice__body table tbody tr td").css({"padding": "5px 0 5px 0","font-size": "12px"});
        $(".kt-invoice-1 .kt-invoice__body").css({"padding": "1.5rem 0"});
        $(".kt-invoice-1 .kt-invoice__head").css({"padding": "50px 0"});
        $(".kt-invoice-1 .kt-invoice__footer").css({"padding": "2.5rem 0"});



        $("#svg").css({
            "padding": "40px",
            "width": "600px",
            "font-family": "sans-serif"
        });


        $('#zn-print').modal('show');
        var pdf = new jsPDF('p', 'pt', 'a4');

        var data = document.getElementById('svg');
        pdf.html(data, {
            callback: function (pdf) {
                var iframe = document.getElementById('znresult');
                iframe.src = pdf.output('datauristring');
            }
        });
    }
    // ENDPRINT

function endSession() {
    $.ajax({
        type: 'POST',
        url: base_url + '/endSession',
        success: function (res) {
            console.log('logout');
            window.location.href = base_url + '/login';
        }
    }).done(function( res ) {

    }).fail(function(res) {
    });
}


function gActAll(proses,action,reloadTable,reloadView) {
    var setTittle = '';
    var setText = '';

    if (proses == 'sendApproval') {
        setTittle = 'Send Approval All';
        setText = 'Yakin akan mengirimkan semua data untuk dimintai persetujuan ?';
    }
    else if(proses == 'giveApproval'){
        setTittle = 'Approval All';
        setText = 'Yakin akan Menyetujui semua data ?';
    }
    else if(proses == 'delete'){
        setTittle = 'Delete All';
        setText = 'Yakin akan menghapus Semua Data ?';
    }
    else if(proses == 'reject'){
        setTittle = 'Reject All';
        setText = 'Yakin Akan Menolak Semua Data ?';
    }
    else if(proses == 'payAmor'){
        setTittle = 'Pay Amortization All';
        setText = 'Yakin akan mengirimkan semua data untuk dimintai persetujuan pembayaran amortisasi ?';
    }
    else if(proses == 'deleteRev'){
        setTittle = 'Approval Delete All';
        setText = 'Yakin akan Menyetujui menghapus Semua Data ?';
    }

    swal.fire({
        title: setTittle,
        text: setText,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F8BD89",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
    }).then(function(result){
        if (result.value) {

            if(proses == 'giveApproval' || proses == 'deleteRev'){

                var SetText = `isi memo atau catatan dibawah ini untuk respon Approval yang anda berikan. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                var SetAction = `
                    <button onclick="gProsesGiveApproval('`+setTittle+`','`+action+`','`+reloadTable+`','','semua')" type="button"
                        class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Kirim</button>`;

                znIconbox("Memo / Catatan",SetText,SetAction,'warning');
            }else{

                loadingPage();
                $.ajax({
                    type: 'POST',
                    url: base_url + action,
                    success: function (res) {
                        endLoadingPage();

                        // CEK SCHEDULE
                        if (res.rc == 88) {
                            var SetText = res.rm;
                            var SetAction = `<button onclick="znClose()" type="button"
                            class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Close</button>`;
                            znIconbox("Aksi Dibatalkan",SetText,SetAction,'danger');

                        }else {
                            toastr.success(setTittle+" Success");
                        }
                    }
                }).done(function( res ) {
                    if (reloadView) {
                        loadNewPage(reloadView);
                    }else{
                        table.ajax.url(reloadTable).load();
                    }

                }).fail(function(res) {
                    endLoadingPage();
                    toastr.warning("Terjadi Kesalahan, Gagal Melakukan "+setTittle);
                });

            }




        }
    });
}

function gActSelected(proses,action,reloadTable,reloadView) {

    var setTittle = '';
    var setText = '';

    if (proses == 'sendApproval') {
        setTittle = 'Send Approval Selected';
        setText = 'Yakin akan mengirimkan data yang dipilih untuk dimintai persetujuan ?';
    }
    else if(proses == 'giveApproval'){
        setTittle = 'Approval Selected';
        setText = 'Yakin akan Menyetujui data yang dipilih ?';
    }
    else if(proses == 'delete'){
        setTittle = 'Delete Selected';
        setText = 'Yakin akan menghapus data yang dipilih ?';
    }
    else if(proses == 'reject'){
        setTittle = 'Reject Selected';
        setText = 'Yakin Akan Menolak Data yang Dipilih ?';
    }
    else if(proses == 'payAmor'){
        setTittle = 'Pay Amortization Selected';
        setText = 'Yakin akan mengirimkan data yang dipilih untuk dimintai persetujuan pembayaran amortisasi ?';
    }
    else if(proses == 'deleteRev'){
        setTittle = 'Approval Delete Selected';
        setText = 'Yakin akan Menyetujui menghapus data yang dipilih ?';
    }

    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        toastr.warning("Minimal Pilih 1 data untuk melakukan '"+setTittle+"' ");
    }else{
        swal.fire({
            title: setTittle,
            text: setText,
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#46C5EF",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                if(proses == 'giveApproval' || proses == 'deleteRev'){

                    var SetText = `isi memo atau catatan dibawah ini untuk respon Approval yang anda berikan. <br><br> <textarea name="inputMemoApprove" id="inputMemoApprove" class="form-control" rows="4" cols="80"></textarea>`;
                    var SetAction = `
                        <button onclick="gProsesGiveApproval('`+setTittle+`','`+action+`','`+reloadTable+`','','satu')" type="button"
                            class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Kirim</button>`;

                    znIconbox("Memo / Catatan",SetText,SetAction,'warning');
                }else{

                    loadingPage();
                    $.ajax({
                        type: 'POST',
                        url: base_url + action,
                        data: {value: value},
                        async: false,
                        success: function (res) {
                            endLoadingPage();

                            // CEK ERROR
                            if (res.rc == 88) {
                                var SetText = res.rm;
                                var SetAction = `<button onclick="znClose()" type="button"
                                class="btn btn-danger btn-elevate btn-pill btn-elevate-air btn-sm">Close</button>`;
                                znIconbox("Aksi Dibatalkan",SetText,SetAction,'danger');

                            }else {
                                toastr.success(setTittle+" Success");
                            }
                        }
                    }).done(function( res ) {

                        if (reloadView) {
                            loadNewPage(reloadView);
                        }else{
                            table.ajax.url(reloadTable).load();
                        }

                    }).fail(function(res) {
                        endLoadingPage();
                        toastr.warning("Terjadi Kesalahan, Gagal Melakukan "+setTittle);
                    });

                }

            }
        });
    }
}


function gCancelPay(proses,action,value,reloadView) {

    var setTittle = '';
    var setText = '';

    if (proses == 'cancelPayAmor') {
        setTittle = 'Cancel Payment';
        setText = 'Yakin akan menggagalkan pembayaran ?';
    }

        swal.fire({
            title: setTittle,
            text: setText,
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#46C5EF",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result){
            if (result.value) {

                    loadingPage();
                    $.ajax({
                        type: 'POST',
                        url: base_url + action,
                        data: {value: value},
                        async: false,
                        success: function (res) {
                            endLoadingPage();
                            toastr.success(setTittle+" Success");
                        }
                    }).done(function( res ) {

                        if (reloadView) {
                            loadNewPage(reloadView);
                        }else{
                            table.ajax.url(reloadTable).load();
                        }

                    }).fail(function(res) {
                        endLoadingPage();
                        toastr.warning("Terjadi Kesalahan, Gagal Melakukan "+setTittle);
                    });


            }
        });

}


function gProsesGiveApproval(setTittle,action,reloadTable,reloadView,type) {
    // loadingModal();
    var SetMemo = $('#inputMemoApprove').val();

    if(type == 'semua'){
        var setData = {SetMemo: SetMemo};
    }else {
        let value = [];
        $('input[type="checkbox"]').each(function(){
            if(this.checked){
                value.push(this.value);
            }
        });
        var setData = {value: value,SetMemo: SetMemo};
    }
    $.ajax({
        type: 'POST',
        url: base_url + action,
        data: setData,
        async: false,
        success: function (res) {
            // endloadingModal();
            // CEK ERROR
            if (res.rc == 88) {
                toastr.info(res.rm);
            }else {
                toastr.success(setTittle+" Success");
            }
        }
    }).done(function( res ) {

        znIconboxClose();
        if (reloadView) {
            loadNewPage(reloadView);
        }else{
            table.ajax.url(reloadTable).load();
        }

    }).fail(function(res) {
        znIconboxClose();
        // endloadingModal();
        toastr.warning("Terjadi Kesalahan, Gagal Melakukan "+setTittle);
    });
}

function endofday(){
    swal.fire({
           title: "Info",
           text: "Set End Of Day",
           type: "info",
           showCancelButton: true,
           confirmButtonText: "Ya",
           cancelButtonText: "Tidak",
           closeOnConfirm: true,
           closeOnCancel: true
        }).then(function(result){
            if (result.value) {
                loadingPage();
                $.ajax({
                    type: 'GET',
                    url: base_url + '/set_end_of_day',
                    success: function (res) {
                        var obj = JSON.parse(res);
                        console.log(obj.msg);
                        endLoadingPage();
                           if(obj.rc==1){
                           swal.fire("Success",obj.msg,"success");
                           }else{
                            swal.fire("Info",obj.msg,"info");
                           }

                        }
                    }).done(function( res ) {
                       endLoadingPage();
                       var obj = JSON.parse(res);
                       if(obj.rc==1){
                           swal.fire("Success",obj.msg,"success");
                           }else{
                            swal.fire("Info",obj.msg,"info");
                           }
                   }).fail(function(res) {
                    endLoadingPage();
                    swal.fire("Error","Terjadi Kesalahan!","error");
                });


            }
        });

}



